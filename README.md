# CRUD Todo App

A Todo App based on Django following the CRUD principle

## Setup and run

1. get python dependencies: `pip install -r requirements.txt` (preferable in a virtual environment)
2. get dependencies `npm install`
3. compile sass files: `npm run sass-build`
4. migrations: `python mangage.py makemigrations` und `python manage.py migrate`
5. run server: `python manage.py runserver`

## License

Copyright © 2021 [Benjamin Heinzerling](https://bitbucket.org/bheinzerling/crud_todo). <br />
This project is [MIT](https://mit-license.org/) licensed.
