from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render, redirect
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.forms.models import model_to_dict
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .models import TodoModel
from .forms import TodoForm

# Create your views here.

class HomeView(View):
    def get(self, request):
        return render(request, "mainapp/home.html")


class LoginView(View):
    def get(self, request, **kwargs):
        form = AuthenticationForm()
        context = {"form": form}
        return render(request, "mainapp/login.html", context=context)

    def post(self, request, **kwargs):
        data = request.POST
        filledForm = AuthenticationForm(data=data)
        if filledForm.is_valid():
            cleanData = filledForm.clean()
            user = authenticate(username=cleanData["username"], password=cleanData["password"])
            if user:
                login(request, user)
                return redirect("/")
            else:
                context = {"form": filledForm, "message": "can't authenticate"}
                return render(request, "mainapp/login.html", context=context)
        else:
            context = {"form": filledForm, "message": "form invalid"}
            return render(request, "mainapp/login.html", context=context)


@method_decorator(login_required, name='dispatch')
class LogoutView(View):
    login_required = True

    def post(self, request):
        logout(request)
        return redirect("/")


class SignupView(View):
    def get(self, request, **kwargs):
        form = UserCreationForm()
        context = {"form": form}
        return render(request, "mainapp/signup.html", context=context)

    def post(self, request, **kwargs):
        filledForm = UserCreationForm(request.POST)
        if filledForm.is_valid():
            user = User.objects.create_user(username=filledForm.cleaned_data["username"], password=filledForm.cleaned_data["password1"])
            user.save()
            login(request, user)
            return redirect("/")
        else:
            context = {"form": filledForm}
            context["message"] = "invalid form"
            return render(request, "mainapp/signup.html", context=context)


@method_decorator(login_required, name='dispatch')
class TodoListView(View):
    login_required = True

    def get(self, request):
        objects = TodoModel.objects.filter(user=request.user)
        context = {"object_list": objects}
        return render(request, "mainapp/list.html", context)


@method_decorator(login_required, name='dispatch')
class TodoCreateView(View):
    login_required = True

    def get(self, request):
        context = {}
        context["form"] = TodoForm()
        return render(request, "mainapp/create.html", context)

    def post(self, request):
        data = request.POST
        filledForm = TodoForm(data)
        newstuff = filledForm.save(commit=False)
        newstuff.user = request.user
        newstuff.save()

        return redirect("listview")


@method_decorator(login_required, name='dispatch')
class DetailView(View):
    login_required = True

    def get(self, request, **kwargs):
        pk = int(kwargs["pk"])
        object = get_object_or_404(TodoModel, pk=pk)
        context = {"object": object}
        return render(request, "mainapp/detail.html", context)

    def post(self, request, **kwargs):
        pk = int(kwargs["pk"])
        object  = TodoModel.objects.filter(pk=pk).first()

        if "button_update" in request.POST.keys():
            return redirect("updateview", str(pk))
        elif "button_delete" in request.POST.keys():
            object.delete()
            return redirect("listview")
        else:
            raise NotImplementedError


@method_decorator(login_required, name='dispatch')
class UpdateView(View):
    login_required = True

    def get(self, request, **kwargs):
        pk = int(kwargs["pk"])
        object = get_object_or_404(TodoModel, pk=pk)
        form = TodoForm(initial=model_to_dict(object))
        context = {"form": form}
        return render(request, "mainapp/update.html", context)

    def post(self, request, **kwargs):
        pk = int(kwargs["pk"])
        object = get_object_or_404(TodoModel, pk=pk)
        filledForm = request.POST
        object.title = filledForm["title"]
        object.description = filledForm["description"]
        if "done" in filledForm:
            object.done = True
        else:
            object.done = False
        object.save()
        return redirect("listview")
