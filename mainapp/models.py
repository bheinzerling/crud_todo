from django.db import models
from django.template.defaultfilters import title
from django.contrib.auth.models import User

# Create your models here.
class TodoModel(models.Model):
    title = models.CharField(max_length=100, default="some title")
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    done = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.title
