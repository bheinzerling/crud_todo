from django import forms
from django.forms import widgets
from django.forms.models import ModelForm
from .models import TodoModel


class ModelFormCssAttrsMixin():
    cssAttrs = {}

    def inject_css_attrs(self):
        # iterate through fields
        for field in self.fields:
            widget = self.fields[field].widget
            widgetClassName = widget.__class__.__name__

            # found widget which should be manipulated?
            if widgetClassName in self.cssAttrs.keys():
                # inject attributes
                attrs = self.cssAttrs[widgetClassName]
                for attr in attrs:
                    if attr in widget.attrs:  # attribute already existing
                        widget.attrs.update[attr] = widget[attr] + " " + attrs[attr]  # append
                    else:  # create attribute since its not existing yet
                        widget.attrs[attr] = attrs[attr]


class TodoForm(ModelFormCssAttrsMixin, forms.ModelForm):
    cssAttrs = {"TextInput": {"class": "input", "type": "text"}, "Textarea": {"class": "textarea"}, "CheckboxInput": {"type": "checkbox", "class": "checkbox"}}

    class Meta:
        model = TodoModel
        fields = ["title", "description", "done"]

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.inject_css_attrs()
