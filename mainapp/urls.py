from django.urls import path
from .views import HomeView, TodoListView, TodoCreateView, DetailView, UpdateView, LoginView, SignupView, LogoutView

urlpatterns = [
    path("", HomeView.as_view(), name="homeview"),
    path("signupview/", SignupView.as_view(), name="signupview"),
    path("loginview/", LoginView.as_view(), name="loginview"),
    path("logoutview/", LogoutView.as_view(), name="logoutview"),
    path("listview/", TodoListView.as_view(), name="listview"),
    path("createview/", TodoCreateView.as_view(), name="createview"),
    path("detailview/<int:pk>", DetailView.as_view(), name="detailview"),
    path("updateview/<int:pk>", UpdateView.as_view(), name="updateview"),
]
